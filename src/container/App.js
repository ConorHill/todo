import React, { Component } from 'react';
import './App';
import Header from '../components/Header/Header'
import TodoForm from '../components/TodoForm/TodoForm'

class App extends Component {
  state = {

  }

  render() {
    return (
      <div className="container-fluid">
        <Header></Header>
        <TodoForm></TodoForm>
      </div>
    );
  }
}

export default App;
