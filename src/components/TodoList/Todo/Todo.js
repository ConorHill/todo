import React from 'react';

const todo = (props) => {
    return (
        <li>{ props.title }</li>
    )
}

export default todo;